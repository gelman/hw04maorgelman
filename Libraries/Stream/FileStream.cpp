#include "FileStream.h"

using namespace msl;

void FileStream::open(const char *path)
{
	this->_file = fopen(path, "w");
}


FileStream& FileStream::operator<<(const char *str)
{
	if (this->_file != nullptr)
	{
		fprintf(this->_file, "%s", str); /*writing to file*/
		fprintf(stdout, "%s", str);
	}
	return *this;
}

FileStream& FileStream::operator<<(int num)
{
	if (this->_file != nullptr)
	{
		fprintf(this->_file, "%d", num); /*writing to file*/
		fprintf(stdout, "%d", num);
	}
	return *this;
}

FileStream& FileStream::operator<<(void(*pf)(FILE*))
{
	pf(this->_file);
	return *this;
}


void endline(FILE* file)
{
	if (file != nullptr)
	{
		fprintf(file, "\n"); /*writing to file*/
		fprintf(stdout, "\n");
	}
}

