#pragma once
#include "OutStream.h"

namespace msl
{
	class FileStream : public OutStream
	{
	public:
		FileStream & operator<<(const char *path);
		FileStream& operator<<(int num);
		FileStream& operator<<(void(*pf)(FILE*));

		void open(const char *str);

	};
}

void endline(FILE* file);
