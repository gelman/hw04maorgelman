#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

namespace msl
{
	class OutStream
	{
	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());

	protected:
		FILE * _file;
	};

}

void endline();