#pragma comment(lib, "..\\Debug\\Stream.lib")

#include "../Stream/OutStream.h"
#include "../Stream/FileStream.h"


int main()
{
	msl::OutStream os;
	msl::FileStream fs;

	os << "I am the Doctor and I'm " << 1500 << " years old" << (&endline) << (&endline);
	
	/*writing to file*/
	fs.open("newFile.txt");
	fs << "I am the Doctor and I'm " << 1500 << " years old" << (&endline);

	getchar();
	return 0;
}



