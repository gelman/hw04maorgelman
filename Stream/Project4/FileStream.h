#pragma once
#include "OutStream.h"

class FileStream : public OutStream
{
public:
	FileStream& operator<<(const char *path);
	FileStream& operator<<(int num);
	FileStream& operator<<(void(*pf)(FILE*));

	void open(const char *str);

};

void endline(FILE* file);