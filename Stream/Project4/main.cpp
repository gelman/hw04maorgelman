#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"

int main(int argc, char **argv)
{
	//part 1:
	OutStream print;
	print << "I am the Doctor and I'm " << 1500 << " years old" << (&endline);

	print << (&endline); //new 

	//part 2:
	FileStream file;
	file.open("sentence.txt");
	file << "I am the Doctor and I'm " << 1500 << " years old" << (&endline);
	
	print << (&endline); //new line for the print

	//part 3:

	OutStreamEncrypted encrypt(3);
	encrypt << "I am the Doctor and I'm " << 1500 << " years old";

	print << (&endline); //new line

	//part 4

	print << (&endline); //new line

	Logger l1;
	Logger l2;

	//using 2 different loggers to see if the static member is working
	l1 << "starting" << (&endline);
	l1 << "printing two numbers:" << (&endline);
	l2 << 1000 << " " << 200 << (&endline);
	l1 << "done" << (&endline);
	l2 << ":)";

	getchar();
	return 0;
}