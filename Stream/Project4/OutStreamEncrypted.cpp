
#include "OutStreamEncrypted.h"
#include <stdlib.h>

#define ARR_SIZE 100
#define CONVERT_TO_CHAR 48

#define HIGHEST_VALUE 126
#define LOWEST_VALUE 32

OutStreamEncrypted::OutStreamEncrypted(int offset)
{
	this->_offset = offset;
}

OutStreamEncrypted::~OutStreamEncrypted()
{

}

char*  OutStreamEncrypted :: encrypt(const char * str, const int offset) const
{
	/*variables definition*/
	int done = 0, res = 0, i = 0;

	char* arr = (char*)malloc(sizeof(char) * ARR_SIZE); /*dynamic allocation*/

	for (i = 0; i < ARR_SIZE && !done; i++)
	{
		if (str[i] != '\0' && str[i] >= LOWEST_VALUE && str[i] <= HIGHEST_VALUE) /*checking if it is a regular case*/
		{
			if (str[i] + offset > HIGHEST_VALUE || str[i] + offset < LOWEST_VALUE) /*checking if we go out of the borders*/
			{
				/*we go out of the boders so we calculate the new value we need*/
				res = str[i] + offset - HIGHEST_VALUE; 
				arr[i] = (char)(res + LOWEST_VALUE);
			}
			else
			{
				arr[i] = (char)(str[i] + offset); /*regular case*/
			}
	
		}
		else if (str[i] != '\0' && ( str[i] < LOWEST_VALUE || str[i] > HIGHEST_VALUE))
		{
			arr[i] = str[i]; /*the chars remain as they are*/
		}
		else
		{
			done = 1; /*done encrypting*/
		}
	}

	arr[i-1] = '\0'; /*putting the \0 in order to know where the string ends*/

	return arr;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	/*encrypting the string*/
	const char* string = encrypt(str, this->_offset);

	fprintf(stdout, "%s", string); 
	
	/*free the memory*/
	delete[] string;
	string = nullptr;

	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	int i = 0;
	char str1[ARR_SIZE] = { 0 };
	char str2[ARR_SIZE] = { 0 };
	char* ps = nullptr;

	/*converting the num to string*/
	while (num > 0)
	{
		str1[i] = (char)(num % 10 + CONVERT_TO_CHAR); /*converting digit to char*/
		i++;
		num = num / 10;
	}
	
	for (int j = 0; j < ARR_SIZE && i - j - 1 >= 0; j++)
	{
		str2[j] = str1[i - j -1];
	}

	/*encrypting the num*/
	ps = encrypt(str2, this->_offset);
	
	fprintf(stdout, "%s", ps);

	/*free the memory*/
	delete[] ps;
	ps = nullptr;

	return *this;
}