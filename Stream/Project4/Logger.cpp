
#include "Logger.h"

unsigned int Logger::_numberOfLines = 0;


Logger::Logger()
{

}

Logger::~Logger()
{

}

void Logger::setStartLine()
{
	/*new line or fisrt line*/
	this->_startLine = true;
	_numberOfLines++;
	printf("LOG %d: ", _numberOfLines);
}

Logger& operator<<(Logger& l, const char *msg)
{
	if (l._numberOfLines == 0) /*fisrt line*/
	{
		l.setStartLine();
	}
	
	l.os << msg; /*printing the msg*/
	l._startLine = false;

	return l;
}

Logger& operator<<(Logger& l, int num)
{
	if (l._numberOfLines == 0) /*fisrt line*/
	{
		l.setStartLine();
	}

	l.os << num; /*printing the num*/
	l._startLine = false;

	return l;
}

Logger& operator<<(Logger& l, void(*pf)())
{
	pf(); /*new line*/
	l.setStartLine();

	return l;
}