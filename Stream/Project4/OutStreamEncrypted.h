#pragma once
#include "FileStream.h"

class OutStreamEncrypted : public OutStream
{
public:
	OutStreamEncrypted(int offset);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);

	char* encrypt(const char * str, const int offset) const;
protected:
	int _offset;
};